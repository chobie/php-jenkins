<?php
namespace Jenkins;

class Config implements \ArrayAccess
{
	protected $indent = 2;
	protected $config;
	
	public function __construct($useDefault = true)
	{
		if($useDefault){
			$this->applyDefaultConfig();
		}
	}
	
	protected function applyDefaultConfig()
	{
		$this["project.actions"] = null;
		$this["project.description"] = "PHP Jenkins Default Configuration";
		$this["project.keepDependencies"] = false;
		$this["project.scm"] = array("@class"=> "hudson.scm.NullSCM");
		$this["project.conRoam"] = true;
		$this["project.disabled"] = false;
		$this["project.blockBuildWhenDownstreamBuilding"] = false;
		$this["project.blockBuildWhenUpstreamBuilding"] = false;
		$this["project.triggers"] = array("@class"=> "vector");
		$this["project.concurrentBuild"] = false;
		$this["project.builders"] = null;
		$this["project.buildWrappers"] = null;
	}

	public static function __parse($obj,$config,$super)
	{
		foreach($obj->children() as $child) {
			if($child->count() == 0) {
				$tmp = $super;
				$name = $child->getName();
				$tmp[] = $name;
				$key = join('.',$tmp);
				
				$attributes = array();
				foreach($child->attributes() as $_key => $value) {
					$attributes["@".$_key] = (string)$value;
				}
				
				if(!empty($attributes)) {
					$config[$key] = $attributes;
				} else {
					$config[$key] = (string)$child;
				}
			} else {
				$super[] = $name;
				self::__parse($child,$config,$super);
			}
		}
	}

	public static function parse($file)
	{
		$config = new self(false);

		if (is_file($file) && is_readable($file)) {
			$xml = simplexml_load_file($file);
			$name = array($xml->getName());
			
			$attributes = array();
			foreach ($xml->attributes() as $key => $value) {
				$attributes["@".$key] = $value;
			}
			if (count($attributes)) {
				$config[$name] = $attributes;
			} else if ($xml->count() == 0) {
				$config[$name] = (string)$xml;
			} else if ($xml->count()) {
				$obj = $xml;
				foreach($obj->children() as $child) {
					self::__parse($obj,$config,$name);
				}
			}
		}

		return $config;
	}
	
	protected function __toXML($config, $depth = 0)
	{
		$buffer = "";

		foreach($config as $key => $item) {
			$buffer .= str_repeat(' ',$depth*$this->indent);
			
			if (isset($item)) {
				$attributes = array();
				if (is_array($item)) {
					foreach($item as $__key => $__value) {
						if($__key[0] == '@') {
							$attributes[substr($__key,1)] = $__value;
							unset($item[$__key]);
						}
					}
				}
				
				if($attributes) {
					$tmp = array();
					foreach($attributes as $__key => $__value) {
						$tmp[] = $__key . '="' . $__value . '"';
					}
					$buffer .= sprintf("<%s %s>",$key, join(' ', $tmp));
				} else {
					$buffer .= sprintf("<%s>",$key);
				}
				
				if(is_array($item) && count($item)) {
					$buffer .= "\n";
					$buffer .= $this->__toXML($item,$depth+1);
				} else if(is_array($item) && empty($item)) {
				} else {
					if ($item === true) {
						$buffer .= "true";
					} else if ($item === false) {
						$buffer .= "false";
					} else {
						$buffer .= $item;
					}
				}
				$buffer .= sprintf("</%s>\n",$key);
			} else {
				$buffer .= sprintf("<%s />\n",$key);
			}
		}
		return $buffer;
	}
	
	public function toXml()
	{
		$buffer = "<?xml version='1.0' encoding='UTF-8'?>\n";
		$buffer .= $this->__toXML($this->config,0);

		return $buffer;
	}
	
	public function offsetExists($offset)
	{
		$tokens = $this->parse_path($offset);
		$offset = $this->config;
		foreach ($tokens as $token) {
			if (isset($offset[$token])) {
				$offset = $offset[$token];
			} else {
				return false;
			}
		}

		return true;
	}
	
	public function offsetGet($offset)
	{
		if($this->offsetExists($offset)) {
			$tokens = $this->parse_path($offset);
			$offset = &$this->config;
			foreach ($tokens as $token) {
				if (isset($offset[$token])) {
					$offset = &$offset[$token];
				} else {
					return false;
				}
			}
			return $offset;
		} else {
			return false;
		}
	}
	
	public function offsetSet($offset, $value)
	{
		$tokens = $this->parse_path($offset);
		$offset = &$this->config;
		$length = count($tokens);
		for ($i = 0; $i < $length; $i++) {
			$token = $tokens[$i];
			
			if ($i+1 == $length) {
				$offset[$token] = $value;
			} else if (!isset($offset[$token])) {
				$offset[$token] = array();
				$offset = &$offset[$token];
			} else if (isset($offset[$token])){
				$offset = &$offset[$token];
			} else {
				throw new \Exception("something worng");
			}
		}
	}
	
	public function offsetUnset($offset)
	{
		if($this->offsetExists($offset)) {
			$tokens = $this->parse_path($offset);
			$offset = &$this->config;
			foreach ($tokens as $token) {
				if (isset($offset[$token])) {
					$offset = &$offset[$token];
				} else {
					return false;
				}
			}
			unset($offset);
			return true;
		} else {
			return false;
		}
	}
	
	protected function parse_path($path)
	{
		$ptr = 0;
		$length = strlen($path);
		$result = array();
		if ($path[0] == '.') {
			error_log("path wrong.");
			return array();
		} else if ($path[$length-1] == '.') {
			error_log("path wrong.");
			return array();
		}
		
		for ($offset = 0;$offset<$length;$offset++) {
			if($path[$offset] == '.') {
				$result[] = substr($path,$ptr,$offset);
				$ptr = $offset+1;
			}
		}
		if($ptr != $offset) {
			$result[] = substr($path,$ptr,$offset);
		}
		return $result;
	}
}

$config = Config::parse("/Users/Shared/Jenkins/Home/jobs/Test/config.xml");
echo $config->toXML();

/*
$config = new \Jenkins\Config();
$config["project.description"] = "Hello World";
var_dump($config->toXML());

$jk = new Jenkins();
$jk->setHomeDirectory("/Users/Shared/Jenkins/Home");
$jk->setEndPoint("http://localhost:8080/");

$jk->loadJobs();
$jobConfig = $jk->getJob("User");
$jk->setJob("User",$jobConfig);
$jk->reload();
$jk->shutdown();
$jk->build();
$jk->clearQueue();

*/